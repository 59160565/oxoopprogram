
public class Game {
	private Board board;
	private player x;
	private player o;
	
	public Game() {
		o = new player('o');
		x = new player('x');
		board = new Board(x,o);
	}
	
	public void	play() {
		showWelcome();
		showTable();
		showTurn();
	}
	
	private void showWelcome() {
		System.out.println("Welcone to OX Game.");
	}
	private void showTable() {
		char[][] table =  board.getTable();
		System.out.println("  1 2 3");
		for (int i=0;i<table.length;i++) {
			System.out.print(i+1);
			for(int j=0;j<table[i].length;j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}
	private void showTurn() {
		System.out.println(board.getCurrent().getName()+" turn..");
		
	}
}
