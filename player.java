
public class player {
	private char name;
	private int win,draw,lose;
	
	public player(char name){
		this.name = name;
		win=0;
		draw=0;
		lose=0;
	}
	public char getName() {
		return name; 
	}
	public int getWin() {
		return win; 
	}
	public int getDraw() {
		return draw; 
	}
	public int getLose() {
		return lose; 
	}
	public void win() {
		win++;
	}
	public void lose() {
		lose++;
	}
	public void draw() {
		draw++;
	}
}
