
public class Board {
	char[][] table = {
			{'-', '-', '-'},
			{'-', '-', '-'},
			{'-', '-', '-'},
		};
	private player x;
	private player o;
	private player winner;
	private player current;
	private int turnCount;
	
	public Board(player x,player o) {
		this.x=x;
		this.o=o;
		current=x;
		winner=null;
		turnCount=0;
	}
	
	public char[][] getTable(){
		return table;
	}
	public player getCurrent() {
		return current;
	}
} 
